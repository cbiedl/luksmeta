Subject: Relax content tests in test suite
Origin: v9-1-g9c550e0 <https://github.com/latchset/luksmeta/commit/v9-1-g9c550e0>
Upstream-Author: Christoph Biedl <debian.axhn@manchmal.in-ulm.de>
Date: Mon Dec 10 14:25:33 2018 +0100

    Starting with version 2.0.5, cryptsetup wipes the full LUKS header
    and fills unused sections with random data, this was introduced
    in commit

        commit c2bce3e93ecee41f661b589ee28f112eb538259e
        Author: Milan Broz <gmazyland@gmail.com>
        Date:   Sun Oct 14 13:11:50 2018 +0200

            Wipe full header areas (including unused) during LUKS format.

    While this is the right thing to do, it breaks luksmeta tests. So
    relax them.

    Bug-Debian: https://bugs.debian.org/915256
    Resolves: https://github.com/latchset/luksmeta/issues/6

--- a/test-lm-assumptions.c
+++ b/test-lm-assumptions.c
@@ -28,7 +28,8 @@
     /* Test the layout state. */
     assert(test_layout((range_t[]) {
         { 0, 1024 },                    /* LUKS header */
-        END(1024),                    /* Rest of the file */
+        { 1024, 3072, true },           /* Keyslot Area */
+        { 0, 0 },
     }));
 
     unlink(filename);
--- a/test-lm-big.c
+++ b/test-lm-big.c
@@ -111,7 +111,7 @@
 
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         { offset + 4096, 4096 },       /* luksmeta slot 0 */
         { offset + 8192, 4096 },       /* luksmeta slot 0 (cont) */
@@ -127,7 +127,7 @@
 
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         END(offset + 4096),            /* Rest of the file */
     }));
--- a/test-lm-init.c
+++ b/test-lm-init.c
@@ -57,7 +57,7 @@
     /* Test the layout state. */
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         END(offset + 4096),            /* Rest of the file */
     }));
@@ -106,7 +106,7 @@
     assert(luksmeta_test(cd) == -ENOENT);
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         END(offset),                   /* Rest of the file */
     }));
 
--- a/test-lm-one.c
+++ b/test-lm-one.c
@@ -49,7 +49,7 @@
     /* Test the layout state. */
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         { offset + 4096, 4096 },       /* luksmeta slot 0 */
         END(offset + 8192),            /* Rest of the file */
@@ -68,7 +68,7 @@
     /* Test the layout state. */
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         END(offset + 4096),            /* Rest of the file */
     }));
--- a/test-lm-two.c
+++ b/test-lm-two.c
@@ -53,7 +53,7 @@
 
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         { offset + 4096, 4096 },       /* luksmeta slot 0 */
         END(offset + 8192),            /* Rest of the file */
@@ -70,7 +70,7 @@
 
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         { offset + 4096, 4096 },       /* luksmeta slot 0 */
         { offset + 8192, 4096 },       /* luksmeta slot 1 */
@@ -88,7 +88,7 @@
     assert(luksmeta_wipe(cd, 0, UUID0) == 0);
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         { offset + 4096, 4096, true }, /* luksmeta slot 0 */
         { offset + 8192, 4096 },       /* luksmeta slot 1 */
@@ -99,7 +99,7 @@
     assert(luksmeta_wipe(cd, 1, UUID1) == 0);
     assert(test_layout((range_t[]) {
         { 0, 1024 },                   /* LUKS header */
-        { 1024, offset - 1024, true }, /* Keyslot Area */
+        { 1024, 3072, true },          /* Keyslot Area */
         { offset, 4096 },              /* luksmeta header */
         END(offset + 4096),            /* Rest of the file */
     }));
